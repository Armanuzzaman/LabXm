<?php
include_once('../../vendor/autoload.php');
use App\Utility\Utility;
use App\ProfilePicture\ImageUploader;

//var_dump($_FILES);

if((isset($_FILES['image'])&& !empty($_FILES['image']['name']))){

    //echo $_FILES['image']['name'];
    $image_name= time().$_FILES['image']['name'];
    //echo $image_name;
    $temporary_location= $_FILES['image']['tmp_name'];
    //echo $temporary_location;


    move_uploaded_file( $temporary_location,'../../Resources/Images/'.$image_name);
    $_POST['image']=$image_name;
    //var_dump($_POST);

}

$profile_picture= new ImageUploader();
$profile_picture->prepare($_POST)->store();



