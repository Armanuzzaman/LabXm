<?php
include_once ('../../vendor/autoload.php');
use App\ProfilePicture\ImageUploader;


$profile_picture= new ImageUploader();
$profile_picture->prepare($_GET);
$profile_picture->recover();