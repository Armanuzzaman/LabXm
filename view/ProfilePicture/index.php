<?php
session_start();
include_once('../../vendor/autoload.php');
use App\ProfilePicture\ImageUploader;
use App\Utility\Utility;
use App\Message\Message;

$info= new ImageUploader();
$info->prepare($_GET);
$singleItem=$info->view();
$object= new ImageUploader();
$allProfile=$object->index();
?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body>
<br/>

<div class="container">
    <h2>All User's Profile Picture</h2>
    <a href="create.php" class="btn btn-primary" role="button">Create again</a>  <a href="trashed.php" class="btn btn-primary" role="button">View Trashed list</a>
    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }
        ?>
    </div>

<div>
    <img src="../../Resources/Images/<?php echo $singleItem->images ?>"alt="image" height="200px" width="200px" class="img-responsive">
</div>

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>SL</th>
                <th>ID</th>
                <th>User Name</th>
                <th>Profile Picture</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allProfile as $info){
                $sl++; ?>
                <img style="display:none;" id="bigpic" src="sourceofPicture" />
                <td><?php echo $sl?></td>
                <td><?php echo $info-> id?></td>
                <td><?php echo $info->name?></td>
                <td><img src="../../Resources/Images/<?php echo $info->images ?>" alt="image" height="100px" width="100px" class="img-responsive"> </td>
                <td>
                    <a href="index.php?id=<?php echo $info-> id ?>" class="btn btn-primary" role="button">ACTIVE</a>

                    <a href="view.php?id=<?php echo $info-> id ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $info-> id ?>"  class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $info->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                    <a href="trash.php?id=<?php echo $info->id ?>"  class="btn btn-info" role="button">Trash</a>

                </td>

            </tr>
            <?php }?>

            </tbody>
        </table>
    </div>

</div>
<script>
    $('#message').show().delay(2000).fadeOut();


    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }



</script>

</body>
</html>
